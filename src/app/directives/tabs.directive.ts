import { Directive, ElementRef, OnInit, OnDestroy, Output, EventEmitter, Input, ContentChildren, QueryList, AfterViewInit, ViewChildren } from '@angular/core';
import * as Materialize from 'materialize-css/dist/js/materialize'; //Importation de Materialize => alias M

@Directive({
  selector: '[tabs]'
})
export class TabsDirective implements OnInit, OnDestroy {

  @Input("tabs") options: any = {};
  @Output("tabsChange") iOut = new EventEmitter();

  private tabsInstance: any;
  private nativeElement: HTMLUListElement;

  constructor(private el: ElementRef<HTMLUListElement>) {}

  private addClickEvent(link: HTMLAnchorElement) {
    link.addEventListener("click", (e: Event) => {
      e.preventDefault();
      const idTarget = link.getAttribute("data-id");
      this.tabsInstance.select(idTarget);
    });
  }

  ngOnInit() {
    this.nativeElement = this.el.nativeElement;
    /**
     * Méthod de Materialize-css afin de récupérer l'instance responsable de la gestion des onglets
     */
    this.tabsInstance = Materialize.Tabs.init(this.nativeElement);
    this.iOut.emit(this.tabsInstance);

    const links = this.nativeElement.querySelectorAll("li > a");

    links.forEach(this.addClickEvent);

  }

  ngOnDestroy() {
    this.tabsInstance.destroy();
  }
}
