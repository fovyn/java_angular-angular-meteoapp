import { Directive, ElementRef, Output, EventEmitter, OnInit } from '@angular/core';
import * as M from 'materialize-css/dist/js/materialize';

@Directive({
  selector: '[appModal]'
})
export class ModalDirective implements OnInit {
  @Output('mInstance') mInstance = new EventEmitter();
  instance: any;

  constructor(private el: ElementRef<HTMLDivElement>) { 
    

  }
  ngOnInit(): void {
    const ne = this.el.nativeElement;
    this.instance = M.Modal.init(ne);

    const buttons = ne.querySelectorAll("button");

    buttons.forEach(btn => btn.addEventListener("click", (e) => {
      this.instance.close();
    }));
    this.mInstance.emit(this.instance);
  }

}
