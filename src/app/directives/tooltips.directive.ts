import { Directive, ElementRef, OnInit, Input } from '@angular/core';
import * as Materialize from 'materialize-css/dist/js/materialize';

@Directive({
  selector: '[tooltips]'
})
export class TooltipsDirective implements OnInit {
  private instance: any;
  private nativeElement: HTMLElement;

  @Input('tooltips') options: {position: string, message: string} = {position: 'bottom', message: 'Message not found!'};

  constructor(private el: ElementRef) { }

  ngOnInit(): void {
    this.nativeElement = this.el.nativeElement;
    this.nativeElement.setAttribute("data-position", this.options.position);
    this.nativeElement.setAttribute("data-tooltip", this.options.message);
    this.instance = Materialize.Tooltip.init(this.nativeElement);
  }

}
