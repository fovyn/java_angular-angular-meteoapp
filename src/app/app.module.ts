import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './components/app/app.component';
import * as M from 'materialize-css/dist/js/materialize';
import { WeatherListComponent } from './components/weather-list/weather-list.component';
import { WeatherCreateComponent } from './components/weather-create/weather-create.component';
import { WeatherEditComponent } from './components/weather-edit/weather-edit.component';
import { ModalDirective } from './directives/modal.directive';
import { TabsDirective } from './directives/tabs.directive';
import { TooltipsDirective } from './directives/tooltips.directive';

M.AutoInit();

@NgModule({
  declarations: [
    AppComponent,
    WeatherListComponent,
    WeatherCreateComponent,
    WeatherEditComponent,
    ModalDirective,
    TabsDirective,
    TooltipsDirective,
  ],
  imports: [
    BrowserModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
