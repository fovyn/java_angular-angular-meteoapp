import { Meteo } from './../../models/meteo';
import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-weather-edit',
  templateUrl: './weather-edit.component.html',
  styleUrls: ['./weather-edit.component.scss']
})
export class WeatherEditComponent implements OnInit {
  @Input() weather: Meteo;
  @Output('edit') editEvent: EventEmitter<Meteo> = new EventEmitter<Meteo>();
  
  constructor() { }

  ngOnInit(): void {
  }

  editAction() {
    this.editEvent.emit(this.weather);
  }
}
