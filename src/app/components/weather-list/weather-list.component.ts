import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Meteo } from 'src/app/models/meteo';

@Component({
  selector: 'app-weather-list',
  templateUrl: './weather-list.component.html',
  styleUrls: ['./weather-list.component.scss']
})
export class WeatherListComponent implements OnInit {
  @Input() items: Array<Meteo> = [];
  @Output("edit") editEvent: EventEmitter<Meteo> = new EventEmitter<Meteo>();

  constructor() { }

  ngOnInit(): void {
  }

  editAction(item: Meteo) {
    this.editEvent.emit(item);
  }
  deleteAction(item: Meteo) {
    const indexItem = this.items.findIndex((i: Meteo) => i.ville == item.ville);
    this.items.splice(indexItem, 1);
  }
}
