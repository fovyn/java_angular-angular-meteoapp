import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Meteo } from 'src/app/models/meteo';

@Component({
  selector: 'app-weather-create',
  templateUrl: './weather-create.component.html',
  styleUrls: ['./weather-create.component.scss']
})
export class WeatherCreateComponent implements OnInit {
  @Output("create") createEvent: EventEmitter<Meteo> = new EventEmitter<Meteo>();

  constructor() { }

  ngOnInit(): void {
  }

  add(townInput: HTMLInputElement, tempInput: HTMLInputElement, descTA: HTMLTextAreaElement) {
    let meteo = new Meteo();
    meteo.ville = townInput.value;
    meteo.temp = parseInt(tempInput.value, null);
    meteo.desc = descTA.value;
    
    this.createEvent.emit(meteo);

    townInput.value = '';
    tempInput.value = '0';
    descTA.value = '';
  }

}
