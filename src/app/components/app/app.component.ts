import { Meteo } from '../../models/meteo';
import { Component, OnInit } from '@angular/core';
import * as Ma from 'materialize-css/dist/js/materialize';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  weathers: Array<Meteo> = [];
  editWeather: Meteo = null;
  mInstance: any;
  M: any;

  ngOnInit(): void {
  }

  createAction(weather: Meteo) {
    this.weathers.push(weather);
    // this.mInstance.open();
    this.M.select('list');
    Ma.toast({html: 'Météo créée'});
  }

  editAction(weather: Meteo) {
    console.log(weather);
    this.M.select('edit');
  }
}
