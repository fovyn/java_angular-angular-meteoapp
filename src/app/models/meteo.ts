export class Meteo {
    private _ville: string = '';
    private _temp: number = 0;
    private _desc: string = '';

    //get et set => Propriété
    get ville(): string { return this._ville; }
    set ville(v: string) { this._ville = v; }
    get desc(): string { return this._desc; }
    set desc(v: string) { this._desc = v; }
    get temp(): number { return this._temp; }
    set temp(v: number){ this._temp = v; }

    get age(): number { return 10; }
}